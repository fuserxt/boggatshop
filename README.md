# Development of the online store "Boggat"                

### Final product: ###
Internet-shop of the firm "Boggat" with a showcase of goods, broken down into categories. There are also various "buns: stock items, etc. The site has: a shopping cart in which the customer adds goods and makes payment using one of the popular payment systems online.

### Functional requirements for the project: ###
1. Shop windows with categories
2. User Cart
3. The letter informing about fulfillment of the order
4. Protection from injections to the database
5. Изменение количества товаров
6. Protection against XSS attacks
7. The payment system has been added