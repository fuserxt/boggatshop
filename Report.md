Отчет о реализации онлайн-магазина "Boggat".
============================================

Выполнил:
-------------------------------
Студент второго курса ПМИ группы БПМИ144 ФКН ВШЭ  
Замылин Сергей  

Формальная постановка задачи:
-------------------------------
_Цитата из wiki.cs.hse.ru:_
>Сервис состоит из: витрины/каталога товаров, разбитых по категориям; корзины, 
в которую пользователь может добавлять покупки; для электронных товаров реализована 
возможность скачать (или получить по почте) и оплата с помощью PayPal (наиболее простой
для интеграции, для проекта достаточно показать работу с разработческим апи PayPal) или 
другими системами оплаты. Для неэлектронных товаров должна быть возможность указать их 
наличие, при покупке количество изменяется. В магазине могут быть промоакции: скидка при 
оформлении заказа по промокоду, скидка при заказе на определенную сумму, временная скидка 
на конкретную категорию товаров.


Реализация формальных критериев:
-------------------------------
**Минимальная функциональность:**

	+Витрина магазина с категориями;
	+Корзина пользователя, в которую можно добавлять или удалять товары;
	+После оформления заказа, письмо с данными о заказе и пользователе отправляется на почту владельца магазина;
	+Приложение защищено от инъекций к базе.
	
**На хорошо:**

	+Приложение работает с неэлектронными товарами, меняется количество доступного товара;
	+Приложение защищено от XSS-атак.
	
**На отлично:**

    +В приложении есть возможность оплатить с помощью сервисов онлайн-платежей;
	+Сервис готов к запуску (по чеклисту выполнено все или почти все).

Работа над проектом:
-------------------


### Модели

* Category 
* Product
* Variation
* ProductImage
* UserCheckout
* UserAddress
* Orders
* CartItem
* Cart

___
Для примера, прикладываю реализацию класса модели Product:

	class Product(models.Model):
		title = models.CharField(max_length=120)
		description = models.TextField(blank=True, null=True)
		price = models.DecimalField(decimal_places=2, max_digits=20)
		active = models.BooleanField(default=True)
		categories = models.ManyToManyField("Category", blank=True)
		default = models.ForeignKey("Category", related_name="default_category", null=True, blank=True)

		object = ProductManager()

		def __str__(self):
			return self.title

		def __unicode__(self):
			return '%s %s' % (self.title, self.active)

		def get_absolute_url(self):
			return reverse("product_detail", kwargs={"pk": self.pk})

		def get_image_url(self):
			img = self.productimage_set.first()
			if img:
				return img.image.url
			return img
			
Наиболее наглядная схема отношений между моделями и базы данных находится в файле DbSchema.png. 

### Шаблоны 


	base.html - базис всех страниц, подключающая head_css.html, javascripts.html, navbar.html;
	home.html - это как index.html;
	about.html - информация о фирме и 'social links';
	forms.html - форма связи с продавцом;
	product_list.html - отображение всех 'активных' товаров на сайте;
	product_detail.html - информация о конкретном товаре, вариации товара, добавление в корзины;
	checkout_user_view.html - продолжить как Continue_as_a_guest/Login_to_continue
	froms_add_addr.hrml - добавление реквезитов адреса;
	view.html - просмотр корзины;
	в папке templates/registration находтся шаблоны для 'django-registration'.
	

### Front end и Back end

В ходе работы было потрачено огромное количество времени на проработку 'user friendly' интерфейса сайта.
Работа над 'front end' составляющей и удобством использования не прекращались в течение всего срока разработки. 
Стек таких технологий как HTML5+CSS+AJAX+JS дали развернуться фантазии.

Конечный результат был достигнут, также при помощи внешних ресурсов:

* Google Fonts - подключение шрифтов;
* Twitter Bootstrap  -  простой и легко настраиваемый HTML, CSS, и Javascript инструментарий;
* Crispy-Forms - приложение, которое позволяет легко настраивать и повторно использовать формы, используя CSS;
* Font Awesome - настраиваемые векторные иконки.

Для получения 'feed back' клиентом, его запрос идентифицируется при помощи urls.py.
Далее информация (аргументы), будут переданы в соответсвующий view.py, которая отработав, 
выполнит некоторые действия, либо вернет страницу.

Пример работы данного алгоритма:
Допустим, что пользователь, находясь на страниче с продуктами 'product_list.html', захотел купить i-й товар.
Ниже показем, как при помощи class-base views, это было описано.

Ссылка на 'product_detail.html' в шаблоне прописывается как:

```python
<a href="{{ product.get_absolute_url }}"><img id="img" class="img-responsive" src="{{ product.get_image_url }}" width="400" height="100"/></a>
```

в классе 'Product' функция:

```python
def get_absolute_url(self):
	return reverse("product_detail", kwargs={"pk": self.pk})
```

urlpatterns в 'urls.py', включает в себя другой модуль URLconf в приложении products:

```python
urlpatterns = [
   	url(r'^$', ProductListView.as_view(), name="products"),
    url(r'(?P<pk>\d+)', ProductDetailView.as_view(), name="product_detail"),
]
```

для того, чтобы в generic views можно было менять, все что угодно, были представлены классы, в
которых основные параметры представлены полями, а функционал наглядно разбит на функции - 
отдельно диспетчеризация, отдельно формирование контекста, отдельно рендеринг шаблона. От такого вида классов
можно наследовать свои 'view', перекрывать в них переменные и функции вместо передачи их в параметрах. 

```python
class ProductDetailView(DetailView):
    model = Product

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailView, self).get_context_data(*args, **kwargs)
        instance = self.get_object()
        context["related"] = Product.object.get_related(instance).order_by("?")
        return context

def product_detail_view_func(request, id):
    product_instance = get_object_or_404(Product, id=id)
    try:
        product_instance = Product.objects.get(id=id)
    except Product.DoesNotExist:
        raise Http404
    except:
        raise Http404

    template = "products/product_detail.html"
    context = {
        "object": product_instance,
    }
    return render(request, template, context)
```

Данный класс был представлен в модуле как 'ProductDetailView.as_view()'.


### Features


* Защита от XSS-атак;
* Регистрация через email;
* Проверка валидности вводимой информации пользователем; 
* Уникальные корзины для зарегистрированных и незарегистрированных пользователей;
* Отправка сообщений через SMTP;
* Для каждого товара отображаюстя родственные ему товары;
* Оплата корзины через PayPal;
* Зарегистрированные пользователи могут отслеживать 'состояние' своих заказов; 
* Вложена душа и огромное количество человеческого ресурса.

