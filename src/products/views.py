from django.shortcuts import render, get_object_or_404
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.http import Http404


from .models import Product, Variation, Category

class CategoryListView(ListView):
	model = Category
	queryset = Category.objects.all()
	template_name = "products/product_list.html"


class CategoryDetailView(DetailView):
    model = Category

def get_context_data(self, *args, **kwargs):
	context = super(CategoryDetailView, self).get_context_data(*args, **kwargs)
	obj = self.get_object()
	product_set = obj.product_set.all()
	default_products = obj.default_category.all()
	products = ( product_set | default_products ).distinct()
	context["products"] = products
	return context


class ProductListView(ListView):
    model = Product

    queryset = Product.object.all()

    def get_context_data(self, *args, **kwargs):
        context = super(ProductListView, self).get_context_data(*args, **kwargs)
        print(context)
        return context


class ProductDetailView(DetailView):
    model = Product

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailView, self).get_context_data(*args, **kwargs)
        instance = self.get_object()
        context["related"] = Product.object.get_related(instance).order_by("?")
        return context

def product_detail_view_func(request, id):
    product_instance = get_object_or_404(Product, id=id)
    try:
        product_instance = Product.objects.get(id=id)
    except Product.DoesNotExist:
        raise Http404
    except:
        raise Http404

    template = "products/product_detail.html"
    context = {
        "object": product_instance,
    }
    return render(request, template, context)


class VariationDetailView(DetailView):
    model = Variation

def variation_detail_view_func(request, id):
    variation_instance = get_object_or_404(Variation, id=id)
    try:
        variation_instance = Variation.objects.get(id=id)
    except:
        raise Http404
    template = "products/product_detail.html"
    context = {
        "vobject": variation_instance
    }
    return render(request, template, context)