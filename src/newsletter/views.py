from django.shortcuts import redirect
from django.core.mail import send_mail
from django.shortcuts import render
from django.conf import settings

from .forms import SignUpForm, ContactForm


def home(request):
    title = "Sign up now"
    form = SignUpForm(request.POST or None)
    context = {
        "template_title": title,
        "form": form,
    }
    if form.is_valid():
        form.save()
        context = {
            "template_title": "Thank you!",
        }

    return render(request, "reg/home.html", context)

def contact(request):
    title = "Contact Us"
    form = ContactForm(request.POST or None)
    if form.is_valid():
        form_email = form.cleaned_data.get("email")
        form_message = form.cleaned_data.get("message")
        form_full_name = form.cleaned_data.get("full_name")
        subject = "Site contact form"
        from_email = settings.EMAIL_HOST_USER
        to_email = [from_email, "fusetestpr@gmail.com"]
        contact_message = "%s: %s via %s"%( form_full_name,
                                            form_message,
                                            form_email )
        try:
            send_mail( subject,
                       contact_message,
                       from_email,
                       to_email,
                       html_message=contact_message,
                       fail_silently=True )
        except:
            #return redirect("home")
            pass

    context = {
        "form": form,
        "title": title,
    }

    return render(request, 'reg/forms.html', context) #and redirect("home")