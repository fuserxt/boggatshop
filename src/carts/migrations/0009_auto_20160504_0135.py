# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carts', '0008_cart_tax_percentage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='tax_percentage',
            field=models.DecimalField(decimal_places=5, max_digits=10, default=0.04),
        ),
    ]
