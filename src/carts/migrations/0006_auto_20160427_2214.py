# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carts', '0005_auto_20160413_2052'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='sub_total',
            field=models.DecimalField(default=0.0, decimal_places=2, max_digits=50),
        ),
    ]
