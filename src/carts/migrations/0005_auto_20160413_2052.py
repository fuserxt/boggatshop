# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carts', '0004_cart_sub_total'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='sub_total',
            field=models.DecimalField(null=True, max_digits=50, decimal_places=2),
        ),
    ]
