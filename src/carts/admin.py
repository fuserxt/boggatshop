from django.contrib import admin

from .models import CartItem, Cart


# you can edit one model lines in another model site through TabularInline
class CartItemInine(admin.TabularInline):
    model = CartItem


class CartAdmin(admin.ModelAdmin):
    inlines = [CartItemInine]

    class Meta:
        model = Cart


admin.site.register(Cart, CartAdmin)