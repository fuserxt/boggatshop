from django.db.models.signals import pre_save, post_save, post_delete
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db import models
from decimal import Decimal


from products.models import Variation


class CartItem(models.Model):
    cart = models.ForeignKey("Cart", default=1)
    item = models.ForeignKey(Variation)
    quantity = models.PositiveIntegerField(default=1)
    # use "pre_save" signal
    line_item_total = models.DecimalField(max_digits=10, decimal_places=2)

    def remove(self):
        return self.item.remove_from_cart()

    def __unicode__(self):
        return self.item.title


def cart_item_pre_save_receiver(sender, instance, *args, **kwargs):
    qty = instance.quantity
    if int(qty) >= 1:
        price = instance.item.get_price()
        line_item_total = Decimal(qty) * Decimal(price)
        instance.line_item_total = line_item_total

pre_save.connect(cart_item_pre_save_receiver, sender=CartItem)


def cart_item_post_save_receiver(sender, instance, *args, **kwargs):
    instance.cart.update_sub_total()

post_save.connect(cart_item_post_save_receiver, sender=CartItem)

post_delete.connect(cart_item_post_save_receiver, sender=CartItem)


class Cart(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)
    items = models.ManyToManyField(Variation, through=CartItem)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    sub_total = models.DecimalField(max_digits=50, decimal_places=2, default=0.00)
    tax_total = models.DecimalField(max_digits=50, decimal_places=2, default=0.00)
    tax_percentage = models.DecimalField(max_digits=10, decimal_places=5, default=0.04)#4% tax percent
    total = models.DecimalField(max_digits=50, decimal_places=2, default=0.00)

    def update_sub_total(self):
        sub_total = 0
        items = self.cartitem_set.all()
        for item in items:
            sub_total += item.line_item_total
        self.sub_total = "%.2f" %(sub_total)
        self.save()

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return str(self.id)


#because we update cart itself we update sub_total
def tax_and_subtotal_receiver(sender, instance, *args, **kwargs):
        #var_tax = Decimal(0.04) #4% tax percent
        tax_total = round(Decimal(instance.sub_total) * Decimal(instance.tax_percentage), 2)
        total = round(Decimal(instance.sub_total) + Decimal(tax_total), 2)
        instance.tax_total = "%.2f" %(tax_total)
        instance.total = "%.2f" %(total)


pre_save.connect(tax_and_subtotal_receiver, sender=Cart)

