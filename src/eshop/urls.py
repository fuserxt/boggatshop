from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

from carts.views import CartView, ItemCountView, CheckoutView, CheckoutFinalView
from orders.views import AddressSelectFormView, UserAddressCreateView, OrderList, OrderDetail

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('shop.urls')),

    url(r'^news/$', 'newsletter.views.home', name='home'),
    url(r'^contacts/$', 'newsletter.views.contact', name='contact'),
    url(r'^about/$', 'eshop.views.about', name='about'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^products/', include('products.urls')),
    url(r'^categories/', include('products.urls_categories')),
    url(r'^cart/$', CartView.as_view(), name='cart'),
    url(r'^cart/count/$', ItemCountView.as_view(), name='item_count'),
    url(r'^orders/$', OrderList.as_view(), name='orders'),
    url(r'^orders/(?P<pk>\d+)/$', OrderDetail.as_view(), name='order_detail'),
    url(r'^checkout/$', CheckoutView.as_view(), name='checkout'),
    url(r'^checkout/address/$', AddressSelectFormView.as_view(), name='order_address'),
    url(r'^checkout/address/add/$', UserAddressCreateView.as_view(), name='user_create_address'),
    url(r'^checkout/final/$', CheckoutFinalView.as_view(), name='checkout_final'),
    url(r'^cache/', include('django_memcached.urls')),

]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, documentary_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, documentary_root=settings.MEDIA_ROOT)
