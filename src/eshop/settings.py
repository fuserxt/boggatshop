import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True

ALLOWED_HOSTS = []


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ls#v)oy0d-d02z9a2am6t**vgd5ji*&6i4i65&@-pvj41^lu6r'

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'registration',
    'shop',
    'products',
    'newsletter',
    'crispy_forms',
    'carts',
    'orders',
    'django_memcached',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'eshop.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, "templates")],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'eshop.wsgi.application'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        }
}

TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True


STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, "static_in_pro", "static_root")

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static_in_pro", "our_static"),
)

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media_in_pro", "media_root")
MEDIAFILES_DIRS = (
    os.path.join(BASE_DIR, "media_in_pro", "our_media"),
)


try:
  from local_settings import *
except ImportError:
  pass


DJANGO_MEMCACHED_REQUIRE_STAFF = True


CRISPY_TEMPLATE_PACK = "bootstrap3"
REGISTRATION_AUTO_LOGIN = True
ACCOUNT_ACTIVATION_DAYS = 7
SITE_ID = 1
LOGIN_REDIRECT_URL = '/'


PAYPAL_TEST = True
PAYPAL_WPP_USER = "sergeyfusezamylin-facilitator_api1.gmial.com"
PAYPAL_WPP_PASSWORD = "8VW5P5WDRHY9HK7K"
PAYPAL_WPP_SIGNATURE = "AIgO.LIK6pcSzA3dVSllj.ZKG2LmAxL2zsbZpxBGRHFffYul1kPt2oNx"
PAYPAL_RECEIVER_EMAIL = "sergeyfusezamylin@gmail.com"

EMAIL_USE_TLS = True
EMAIL_HOST = "smtp.gmail.com"
EMAIL_HOST_USER = "fusetestpr@gmail.com"
EMAIL_HOST_PASSWORD = 'sergey449650051'
EMAIL_PORT = 587
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'


#Braintree payments
BRAINTREE_ENV = "sandbox"
BRAINTREE_PUBLIC_KEY = "y4zy4j5j9jns54fs"
BRAINTREE_PRIVATE_KEY = "9c64522bb81fa6a455c43a52780aa466"
BRAINTREE_MERCHANT_ID = "kt52cccc8c6mp7xv"
