from django.contrib.auth import get_user_model
from .models import UserAddress
from django import forms

USER = get_user_model()


class GuestCheckoutForm(forms.Form):
    email = forms.EmailField()
    email2 = forms.EmailField(label="Verify email")

    def clean_email2(self):
        email = self.cleaned_data.get("email")
        email2 = self.cleaned_data.get("email2")

        if email == email2:
            user_exists = USER.objects.filter(email=email).count()
            if user_exists != 0:
                raise forms.ValidationError("This User already exists. Please login in")
            return email2
        else:
            raise forms.ValidationError("Please, check you email")


class AddressForm(forms.Form):
    #allows base of a model or queryset from model
    #widgat - for dropdowm choices
    shipping_address = forms.ModelChoiceField(queryset=UserAddress.objects.filter(type="shipping"),
                                              empty_label=None,
                                              widget=forms.RadioSelect)


class UserAddressForm(forms.ModelForm):

    class Meta:
         model = UserAddress
         fields = [
             'city',
             'state',
             'street',
             'type',
             'zipcode'
         ]