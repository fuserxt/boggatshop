from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.edit import FormView, CreateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.http import Http404

from .forms import AddressForm, UserAddressForm
from .mixins import CartOrderMixin, LoginRequiredMixin
from .models import UserAddress, UserCheckout, Orders


class OrderDetail(DetailView):
    model = Orders

    def dispatch(self, request, *args, **kwargs):
        try:
            user_check_id = self.request.session.get("user_checkout_id")
            user_checkout = UserCheckout.objects.get(id=user_check_id)
        except UserCheckout.DoesNotExist:
            user_checkout = UserCheckout.objects.get(user=request.user)
        except:
            user_checkout = None

        obj = self.get_object()
        if obj.user == user_checkout and user_checkout is not None:
            return super(OrderDetail, self).dispatch(request, *args, **kwargs)
        else:
            raise Http404


class OrderList(LoginRequiredMixin, ListView):
    queryset = Orders.objects.all()

    def get_queryset(self):
        user_check_id = self.request.user.id
        user_checkout = UserCheckout.objects.get(id=user_check_id)
        return super(OrderList, self).get_queryset().filter(user=user_checkout)


class AddressSelectFormView(CartOrderMixin, FormView):
    form_class = AddressForm
    template_name = "orders/address_select.html"

    def dispatch(self, *args, **kwargs):
        shipping_address_check = self.get_addresses()

        if shipping_address_check.count() == 0:
            print("kek1")
            return redirect("user_create_address")
        else:
            return super(AddressSelectFormView, self).dispatch(*args, **kwargs)

    def get_addresses(self, *args, **kwargs):
        user_check_id = self.request.session.get("user_checkout_id")
        user_checkout = UserCheckout.objects.get(id=user_check_id)

        shipping_address_check = UserAddress.objects.filter(
            user=user_checkout,
            type='shipping',
        )
        return shipping_address_check

    #update queryset field in /checkout/address
    def get_form(self, *args, **kwargs):
        form = super(AddressSelectFormView, self).get_form(*args, **kwargs)
        #kludge for anonymous user
        shipping_address_check = self.get_addresses()
        #create new queryset
        form.fields["shipping_address"].queryset = shipping_address_check
        return form

    def form_valid(self, form, *args, **kwargs):
        shipping_add = form.cleaned_data["shipping_address"]
        order = self.get_order()
        order.shipping_address = shipping_add
        order.save()
        #self.request.session["billing_address_id"] = billing_add.id
        #self.request.session["shipping_address_id"] = shipping_add.id
        return super(AddressSelectFormView, self).form_valid(form, *args, **kwargs)

    def get_success_url(self, *args, **kwargs):
        return "/checkout/"


class UserAddressCreateView(CreateView):
    form_class = UserAddressForm
    template_name = "reg/forms_add_addr.html"
    success_url = "/checkout/address/"

    def get_checkout_user(self):
        user_check_id = self.request.session.get("user_checkout_id")
        user_checkout = UserCheckout.objects.get(id=user_check_id)
        return user_checkout

    def form_valid(self, form, *args, **kwargs):
        form.instance.user = self.get_checkout_user()
        return super(UserAddressCreateView, self).form_valid(form, *args, **kwargs)













