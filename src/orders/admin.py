from django.contrib import admin
from .models import UserCheckout, UserAddress, Orders


admin.site.register(Orders)
admin.site.register(UserCheckout)
admin.site.register(UserAddress)