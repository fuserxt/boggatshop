from django.db.models.signals import pre_save, post_save
from django.contrib.auth import get_user_model
from django.conf import settings
from django.core.urlresolvers import reverse


from carts.models import Cart
from django.db import models
from decimal import Decimal

import braintree

if settings.DEBUG:
    braintree.Configuration.configure(
                                        braintree.Environment.Sandbox,
                                        merchant_id=settings.BRAINTREE_MERCHANT_ID,
                                        public_key=settings.BRAINTREE_PUBLIC_KEY,
                                        private_key=settings.BRAINTREE_PRIVATE_KEY,
    )

#first in database, second - displayed
ADDRESS_TYPE = (
    ('shipping', 'Shipping'),
)

ORDER_STATIC_CHOICES = (
    ('created', 'CREATED'),
    ('paid', 'PAID'),
    ('shipped', 'SHIPPED'),
    ('refunded', 'REFUNDED ')

)


class UserCheckout(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, blank=True, null=True)
    #for unique users in database
    email = models.EmailField(unique=True)
    #for braintree payments
    braintree_id = models.CharField(max_length=120, null=True, blank=True)

    def __str__(self):
        return self.email

    @property
    def get_braintree_id(self):
        instance = self
        if not instance.braintree_id:
            result = braintree.Customer.create({
            "email" : instance.email,
            })
            if result.is_success:
                instance.braintree_id = result.customer.id
                instance.save()
        return instance.braintree_id

    def get_client_token(self):
        customer_id = self.get_braintree_id
        if customer_id:
            client_token = braintree.ClientToken.generate({
                "customer_id": customer_id,
            })
            return client_token
        return None


def update_braintree_id(sender, instance, *args, **kwargs):
    if not instance.braintree_id:
        instance.get_braintree_id

post_save.connect(update_braintree_id, sender=UserCheckout)


class UserAddress(models.Model):
    user = models.ForeignKey(UserCheckout)
    type = models.CharField(max_length=120, choices=ADDRESS_TYPE)
    street = models.CharField(max_length=120)
    city = models.CharField(max_length=120)
    state = models.CharField(max_length=120)
    zipcode = models.CharField(max_length=120)

    def get_address(self):
        return "%s, %s, %s, %s" %(self.street, self.city, self.state, self.zipcode)

    def __str__(self):
        return self.street


class Orders(models.Model):
    status = models.CharField(max_length=120, choices=ORDER_STATIC_CHOICES, default='created')
    cart = models.ForeignKey(Cart)
    user = models.ForeignKey(UserCheckout, null=True)
    shipping_address = models.ForeignKey(UserAddress, related_name="shipping_address", null=True)
    shipping_total_price = models.DecimalField(max_digits=50, decimal_places=2,  default=5.00)
    order_total = models.DecimalField(max_digits=50, decimal_places=2)
    order_id = models.CharField(max_length=20, null=True, blank=True)

    class Meta:
        ordering = ['-id']

    def mark_completed(self, order_id=None):
        self.status = 'paid'
        if not self.order_id and order_id:
            self.order_id = order_id
        self.save()

    def get_absolute_url(self):
        return reverse("order_detail", kwargs={"pk": self.pk})

    def __str__(self):
        return str(self.cart.id)


def order_pre_save(sender, instance, *args, **kwargs):
    #instance is default for var
    instance.order_total = Decimal(instance.shipping_total_price) + Decimal(instance.cart.total)

pre_save.connect(order_pre_save, sender=Orders)