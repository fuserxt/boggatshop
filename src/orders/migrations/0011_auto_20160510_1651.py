# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0010_auto_20160507_1207'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='orders',
            options={'ordering': ['-id']},
        ),
        migrations.RemoveField(
            model_name='orders',
            name='billing_address',
        ),
        migrations.AlterField(
            model_name='orders',
            name='status',
            field=models.CharField(max_length=120, default='created', choices=[('created', 'CREATED'), ('paid', 'PAID'), ('shipped', 'SHIPPED'), ('refunded', 'REFUNDED ')]),
        ),
        migrations.AlterField(
            model_name='useraddress',
            name='type',
            field=models.CharField(max_length=120, choices=[('shipping', 'Shipping')]),
        ),
        migrations.AlterField(
            model_name='useraddress',
            name='zipcode',
            field=models.CharField(max_length=120),
        ),
    ]
