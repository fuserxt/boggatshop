# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0005_auto_20160503_2202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orders',
            name='shipping_total_price',
            field=models.DecimalField(default=5.0, max_digits=50, decimal_places=2),
        ),
    ]
