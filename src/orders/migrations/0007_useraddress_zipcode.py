# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0006_auto_20160504_1253'),
    ]

    operations = [
        migrations.AddField(
            model_name='useraddress',
            name='zipcode',
            field=models.IntegerField(default=0, max_length=120),
        ),
    ]
