# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0007_useraddress_zipcode'),
    ]

    operations = [
        migrations.AddField(
            model_name='orders',
            name='status',
            field=models.CharField(default='created', choices=[('created', 'CREATED'), ('completed', 'COMPLETED')], max_length=120),
        ),
    ]
