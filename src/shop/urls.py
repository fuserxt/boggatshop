from django.conf.urls import url, patterns
from django.conf import settings

from shop import views

urlpatterns = [
    #url(r'^$', views.tshirts_list, name='tshirts_list'),
    #url(r'^product/(?P<pk>[0-9]+)/$', views.tshirts_detail, name='tshirts_detail'),

    #стартовая страница
   url(r'^$', views.site, name='site'),
    #футболки
    url(r'^tshirts/$', views.tshirts, name='tshirts'),
    url(r'^tshirts/(?P<pk>[0-9]+)/$', views.tshirts_detail, name='tshirts_detail'),
    #аксесуары
    url(r'^accessories/$', views.accessories, name='accessories'),
    url(r'^accessories/(?P<pk>[0-9]+)/$', views.accessories_detail, name='accessories_detail'),
    #инфа
    url(r'^aboutus/$', views.aboutus, name='aboutus'),
    #контакты
    url(r'^contact/$', views.contact, name='contact'),

]

urlpatterns += patterns('',
                        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT})
                       )
