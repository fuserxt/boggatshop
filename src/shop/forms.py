from django import forms
from .models import TShirt

class TShirtForm(forms.ModelForm):
    class Meta:
        model = TShirt
        fields = ['name', 'colour', 'description', 'price_in_rub']
