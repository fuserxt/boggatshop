from django.shortcuts import render, get_object_or_404, redirect
#from .models import Accessories, TShirt


#request  - то что мы получаем от пользователя при запросе

#стартовая
def site(request):
    #object_l = TShirts.objects.all()
    return redirect("home")
    #return render(request, 'reg/home.html.html', {})

#фктболки
def tshirts(request):
    object_l = TShirt.objects.all()
    return render(request, 'tshirts/tshirts_list.html', {'object_l': object_l})
def tshirts_detail(request, pk):
    object_d = get_object_or_404(TShirt, pk=pk)
    return render(request, 'tshirts/tshirts_detail.html', {'object_d': object_d})

#аксесуары
def accessories(request):
    object_l = Accessories.objects.all()
    return render(request, 'accessories/accessories_list.html', {'object_l': object_l})
def accessories_detail(request, pk):
    object_d = get_object_or_404(Accessories, pk=pk)
    return render(request, 'accessories/accessories_detail.html', {'object_d': object_d})

#инфа
def aboutus(request):
    return render(request, 'aboutus/aboutus.html', {})

#контакты
def contact(request):
    return render(request, 'contact/contact.html', {})


