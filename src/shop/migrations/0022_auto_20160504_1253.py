# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0021_auto_20160504_0135'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accessories',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 4, 9, 53, 50, 70554, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='collection',
            name='date_begin',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 4, 9, 53, 50, 67547, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='collection',
            name='date_end',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 4, 9, 53, 50, 67610, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='publisher',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 4, 9, 53, 49, 671195, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tshirt',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 4, 9, 53, 50, 68819, tzinfo=utc)),
        ),
    ]
