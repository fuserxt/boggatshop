# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Accessories',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=30, default='')),
                ('slug', models.SlugField(max_length=150, default='')),
                ('gender', models.CharField(max_length=1, choices=[('M', 'Male'), ('F', 'Female')], default='')),
                ('colour', models.CharField(max_length=300, default='')),
                ('description', models.TextField(default='')),
                ('photo', models.ImageField(blank=True, upload_to='')),
                ('manufacturer', models.CharField(blank=True, max_length=300, default='')),
                ('price_in_rubs', models.DecimalField(decimal_places=2, max_digits=6)),
                ('pub_date', models.DateTimeField(default=datetime.datetime(2016, 4, 10, 22, 24, 11, 179256, tzinfo=utc))),
            ],
            options={
                'verbose_name_plural': 'Accessories',
                'verbose_name': 'Accessories',
            },
        ),
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name_of_collection', models.CharField(max_length=300)),
                ('date_begin', models.DateTimeField(default=datetime.datetime(2016, 4, 10, 22, 24, 11, 176000, tzinfo=utc))),
                ('date_end', models.DateTimeField(default=datetime.datetime(2016, 4, 10, 22, 24, 11, 176076, tzinfo=utc))),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=120)),
                ('description', models.TextField(blank=True, null=True)),
                ('price', models.DecimalField(decimal_places=2, max_digits=20)),
                ('active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Publisher',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('nam', models.CharField(max_length=30)),
                ('address', models.CharField(max_length=50)),
                ('city', models.CharField(max_length=60)),
                ('pub_date', models.DateTimeField(default=datetime.datetime(2016, 4, 10, 22, 24, 10, 781586, tzinfo=utc))),
            ],
        ),
        migrations.CreateModel(
            name='TShirt',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=30, default='')),
                ('slug', models.SlugField(max_length=150, default='')),
                ('gender', models.CharField(max_length=1, choices=[('M', 'Male'), ('F', 'Female')], default='')),
                ('colour', models.CharField(max_length=300, default='')),
                ('description', models.TextField(default='')),
                ('photo', models.ImageField(blank=True, upload_to='')),
                ('manufacturer', models.CharField(blank=True, max_length=300, default='')),
                ('price_in_rubs', models.DecimalField(decimal_places=2, max_digits=6)),
                ('pub_date', models.DateTimeField(default=datetime.datetime(2016, 4, 10, 22, 24, 11, 177411, tzinfo=utc))),
                ('collection', models.ManyToManyField(to='shop.Collection')),
                ('publisher', models.ForeignKey(to='shop.Publisher')),
            ],
            options={
                'verbose_name_plural': 'T-Shirts',
                'verbose_name': 'T-Shirt',
            },
        ),
        migrations.AddField(
            model_name='accessories',
            name='collection',
            field=models.ManyToManyField(to='shop.Collection'),
        ),
        migrations.AddField(
            model_name='accessories',
            name='publisher',
            field=models.ForeignKey(to='shop.Publisher'),
        ),
    ]
