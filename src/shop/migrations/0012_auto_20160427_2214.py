# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0011_auto_20160413_2052'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accessories',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 19, 14, 34, 603382, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='collection',
            name='date_begin',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 19, 14, 34, 600319, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='collection',
            name='date_end',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 19, 14, 34, 600380, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='publisher',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 19, 14, 34, 209491, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tshirt',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 19, 14, 34, 601621, tzinfo=utc)),
        ),
    ]
