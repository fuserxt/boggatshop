# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0014_auto_20160428_0018'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accessories',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 21, 47, 33, 86569, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='collection',
            name='date_begin',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 21, 47, 33, 83520, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='collection',
            name='date_end',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 21, 47, 33, 83585, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='publisher',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 21, 47, 32, 696388, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tshirt',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 21, 47, 33, 84792, tzinfo=utc)),
        ),
    ]
