# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0026_auto_20160507_1207'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='accessories',
            name='collection',
        ),
        migrations.RemoveField(
            model_name='accessories',
            name='publisher',
        ),
        migrations.DeleteModel(
            name='Product',
        ),
        migrations.RemoveField(
            model_name='tshirt',
            name='collection',
        ),
        migrations.RemoveField(
            model_name='tshirt',
            name='publisher',
        ),
        migrations.DeleteModel(
            name='Accessories',
        ),
        migrations.DeleteModel(
            name='Collection',
        ),
        migrations.DeleteModel(
            name='Publisher',
        ),
        migrations.DeleteModel(
            name='TShirt',
        ),
    ]
