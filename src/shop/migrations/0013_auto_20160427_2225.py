# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0012_auto_20160427_2214'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accessories',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 19, 25, 52, 398970, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='collection',
            name='date_begin',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 19, 25, 52, 395226, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='collection',
            name='date_end',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 19, 25, 52, 395290, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='publisher',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 19, 25, 52, 8818, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tshirt',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 27, 19, 25, 52, 396488, tzinfo=utc)),
        ),
    ]
