# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0025_auto_20160506_2202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accessories',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 7, 9, 7, 46, 985695, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='collection',
            name='date_begin',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 7, 9, 7, 46, 982729, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='collection',
            name='date_end',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 7, 9, 7, 46, 982792, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='publisher',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 7, 9, 7, 46, 581792, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tshirt',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 7, 9, 7, 46, 983972, tzinfo=utc)),
        ),
    ]
